package common

/**
 * GOPHER: Santi ʕ◔ϖ◔ʔ
 */

import (
	"crypto/rsa"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"
	"github.com/gorilla/context"
)

type AppClaims struct {
	Usuario string `json:"Usuario"`
	Nombre  string `json:"Nombre"`
	Rol     int    `json:"Rol"`
	Centro  int    `json:"Centro"`
	jwt.StandardClaims
}

type verifyFuncsType struct{}

var VerifyFuncs = new(verifyFuncsType)

type Claim struct{}

var ClaimsCommon = new(Claim)

const (
	privKeyPath = "keys/private.rsa"
	pubKeyPath  = "keys/public.rsa.pub"
)

var (
	//verifyKey, signKey []byte
	verifyKey *rsa.PublicKey
	signKey   *rsa.PrivateKey
)

//Read key files
func initKeys() {
	signBytes, err := ioutil.ReadFile(privKeyPath)
	if err != nil {
		log.Fatalf("[initKeys]: %s\n", err)
	}

	signKey, err = jwt.ParseRSAPrivateKeyFromPEM(signBytes)
	if err != nil {
		log.Fatalf("[initKeys]: %s\n", err)
	}

	verifyBytes, err := ioutil.ReadFile(pubKeyPath)
	if err != nil {
		log.Fatalf("[initKeys]: %s\n", err)
	}

	verifyKey, err = jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
	if err != nil {
		log.Fatalf("[initKeys]: %s\n", err)
	}
}

//GenerateJWT genera un nuevo JWT basado en los claims que recibe como parametros
func GenerateJWT(user, name string, role, centro int) (string, error) {
	//Create the claims
	claims := AppClaims{
		user,
		name,
		role,
		centro,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Minute * 30).Unix(),
			Issuer:    "admin",
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)

	ss, err := token.SignedString(signKey)
	if err != nil {
		return "", err
	}

	return ss, nil
}

//Authorize esta funcion exportada verifica que el jwt enviado en el header si sea valido
func Authorize(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	token, err := request.ParseFromRequestWithClaims(r, request.OAuth2Extractor, &AppClaims{}, func(token *jwt.Token) (interface{}, error) {
		return verifyKey, nil
	})
	if err != nil {
		switch err.(type) {

		case *jwt.ValidationError: // JWT validation error
			vErr := err.(*jwt.ValidationError)

			switch vErr.Errors {
			case jwt.ValidationErrorExpired: //JWT expired
				DisplayAppError(
					w,
					err,
					"Access Token is expired, get a new Token",
					401,
				)
				return

			default:
				DisplayAppError(w,
					err,
					"Error while parsing the Access Token!",
					500,
				)
				return
			}

		default:
			DisplayAppError(w,
				err,
				"Error while parsing Access Token!",
				500)
			return
		}
	}

	if token.Valid {
		// Set user name to HTTP context
		context.Set(r, "user", token.Claims.(*AppClaims).Usuario)
		next(w, r)
	} else {
		DisplayAppError(
			w,
			err,
			"Invalid Access Token",
			401,
		)
	}
}

//GetCentro esta funcion exportada retorna el codigo del centro de los claims del JWT
func (vf *verifyFuncsType) GetCentro(token string) (int, error) {
	parsedToken, err := jwt.ParseWithClaims(token[7:], &AppClaims{}, func(token *jwt.Token) (interface{}, error) {
		return verifyKey, nil
	})
	if err != nil {
		return -1, err
	} else if parsedToken.Valid {
		return parsedToken.Claims.(*AppClaims).Centro, nil
	}
	return -1, nil
}

//GetUsername obtiene el nombre de usuario de los claims del JWT
func (vf *verifyFuncsType) GetUserName(token string) (string, error) {
	parsedToken, err := jwt.ParseWithClaims(token[7:], &AppClaims{}, func(token *jwt.Token) (interface{}, error) {
		return verifyKey, nil
	})
	if err != nil {
		return "", err
	} else if parsedToken.Valid {
		return parsedToken.Claims.(*AppClaims).Usuario, nil
	}
	return "", nil
}

// TokenFromAuthHeader is a "TokenExtractor" that takes a given request and extracts
// the JWT token from the Authorization header.
func TokenFromAuthHeader(r *http.Request) (string, error) {
	// Look for an Authorization header
	if ah := r.Header.Get("Authorization"); ah != "" {
		// Should be a bearer token
		if len(ah) > 6 && strings.ToUpper(ah[0:6]) == "BEARER" {
			return ah[7:], nil
		}
	}
	return "", errors.New("No token in the HTTP request")
}
