package routers

import (
	"bitbucket.org/colombia9503/AgriculturaDePrecision/common"
	"bitbucket.org/colombia9503/AgriculturaDePrecision/controllers"
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

//SetTipoMuestrasRouter router for detalleResultados endpoints
func SetTipoMuestrasRouter(router *mux.Router) *mux.Router {
	tipoMuestrasRouter := mux.NewRouter()
	tipoMuestrasRouter.HandleFunc("/api/tipomuestras", controllers.TipoMuestras.Create).Methods("POST")
	tipoMuestrasRouter.HandleFunc("/api/tipomuestras", controllers.TipoMuestras.Get).Methods("GET")
	tipoMuestrasRouter.HandleFunc("/api/tipomuestras/{id}", controllers.TipoMuestras.GetOne).Methods("GET")
	tipoMuestrasRouter.HandleFunc("/api/tipomuestras/{id}", controllers.TipoMuestras.Update).Methods("PUT")
	tipoMuestrasRouter.HandleFunc("/api/tipomuestras/{id}", controllers.TipoMuestras.Delete).Methods("DELETE")
	router.PathPrefix("/api/tipomuestras").Handler(
		negroni.New(
			negroni.HandlerFunc(common.Authorize),
			negroni.Wrap(tipoMuestrasRouter),
		))
	return router
}
