package routers

import "github.com/gorilla/mux"

//InitRouters init all routers
func InitRouters() *mux.Router {
	router := mux.NewRouter().StrictSlash(false)
	router = SetAuthRouter(router)
	router = SetCentrosRouters(router)
	router = SetDetalleResultadosRouter(router)
	router = SetMuestrasRouter(router)
	router = SetResultadosRouter(router)
	router = SetTipoMuestrasRouter(router)
	router = SetTipoPruebasRouter(router)
	router = SetUsuariosRouter(router)
	router = SetGraphResultadosRouter(router)
	return router
}
