package routers

import (
	"bitbucket.org/colombia9503/AgriculturaDePrecision/common"
	"bitbucket.org/colombia9503/AgriculturaDePrecision/controllers"
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

//SetMuestrasRouter router for detalleResultados endpoints
func SetMuestrasRouter(router *mux.Router) *mux.Router {
	muestrasRouter := mux.NewRouter()
	muestrasRouter.HandleFunc("/api/muestras", controllers.Muestras.Create).Methods("POST")
	muestrasRouter.HandleFunc("/api/muestras", controllers.Muestras.Get).Methods("GET")
	muestrasRouter.HandleFunc("/api/muestras/{id}", controllers.Muestras.GetOne).Methods("GET")
	muestrasRouter.HandleFunc("/api/muestras/{id}", controllers.Muestras.Update).Methods("PUT")
	muestrasRouter.HandleFunc("/api/muestras/{id}", controllers.Muestras.Delete).Methods("DELETE")
	router.PathPrefix("/api/muestras").Handler(
		negroni.New(
			negroni.HandlerFunc(common.Authorize),
			negroni.Wrap(muestrasRouter),
		))
	return router
}
