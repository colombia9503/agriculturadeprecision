package routers

import (
	"bitbucket.org/colombia9503/AgriculturaDePrecision/common"
	"bitbucket.org/colombia9503/AgriculturaDePrecision/controllers"
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

//SetDetalleResultadosRouter router for detalleResultados endpoints
func SetDetalleResultadosRouter(router *mux.Router) *mux.Router {
	detalleResultadosRouter := mux.NewRouter()
	detalleResultadosRouter.HandleFunc("/api/detalleresult", controllers.DetalleResultados.Create).Methods("POST")
	detalleResultadosRouter.HandleFunc("/api/detalleresult", controllers.DetalleResultados.Get).Methods("GET")
	detalleResultadosRouter.HandleFunc("/api/detalleresult/{id}", controllers.DetalleResultados.GetOne).Methods("GET")
	detalleResultadosRouter.HandleFunc("/api/detalleresult/{id}", controllers.DetalleResultados.Update).Methods("PUT")
	detalleResultadosRouter.HandleFunc("/api/detalleresult/{id}", controllers.DetalleResultados.Delete).Methods("DELETE")
	router.PathPrefix("/api/detalleresult").Handler(
		negroni.New(
			negroni.HandlerFunc(common.Authorize),
			negroni.Wrap(detalleResultadosRouter),
		))
	return router
}
