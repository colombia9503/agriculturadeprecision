package routers

import (
	"bitbucket.org/colombia9503/AgriculturaDePrecision/common"
	"bitbucket.org/colombia9503/AgriculturaDePrecision/controllers"
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

//SetTipoPruebasRouter router for detalleResultados endpoints
func SetTipoPruebasRouter(router *mux.Router) *mux.Router {
	tipoPruebasRouter := mux.NewRouter()
	tipoPruebasRouter.HandleFunc("/api/tipopruebas", controllers.TipoPruebas.Create).Methods("POST")
	tipoPruebasRouter.HandleFunc("/api/tipopruebas", controllers.TipoPruebas.Get).Methods("GET")
	tipoPruebasRouter.HandleFunc("/api/tipopruebas/{id}", controllers.TipoPruebas.GetOne).Methods("GET")
	tipoPruebasRouter.HandleFunc("/api/tipopruebas/{id}", controllers.TipoPruebas.Update).Methods("PUT")
	tipoPruebasRouter.HandleFunc("/api/tipopruebas/{id}", controllers.TipoPruebas.Delete).Methods("DELETE")
	router.PathPrefix("/api/tipopruebas").Handler(
		negroni.New(
			negroni.HandlerFunc(common.Authorize),
			negroni.Wrap(tipoPruebasRouter),
		))
	return router
}
