package routers

import (
	"bitbucket.org/colombia9503/AgriculturaDePrecision/controllers"
	"github.com/gorilla/mux"
)

func SetAuthRouter(router *mux.Router) *mux.Router {
	router.HandleFunc("/auth/login", controllers.Auth.Login).Methods("POST")
	return router
}
