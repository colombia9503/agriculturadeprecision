package routers

import (
	"bitbucket.org/colombia9503/AgriculturaDePrecision/common"
	"bitbucket.org/colombia9503/AgriculturaDePrecision/controllers"
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

//SetResultadosRouter router for detalleResultados endpoints
func SetResultadosRouter(router *mux.Router) *mux.Router {
	resultadosRouter := mux.NewRouter()
	resultadosRouter.HandleFunc("/api/resultados", controllers.Resultados.Create).Methods("POST")
	resultadosRouter.HandleFunc("/api/resultados", controllers.Resultados.Get).Methods("GET")
	resultadosRouter.HandleFunc("/api/resultados/{id}", controllers.Resultados.GetOne).Methods("GET")
	resultadosRouter.HandleFunc("/api/resultados/{id}", controllers.Resultados.Update).Methods("PUT")
	resultadosRouter.HandleFunc("/api/resultados/{id}", controllers.Resultados.Delete).Methods("DELETE")
	router.PathPrefix("/api/resultados").Handler(
		negroni.New(
			negroni.HandlerFunc(common.Authorize),
			negroni.Wrap(resultadosRouter),
		))
	return router
}
