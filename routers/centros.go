package routers

import (
	"bitbucket.org/colombia9503/AgriculturaDePrecision/common"
	"bitbucket.org/colombia9503/AgriculturaDePrecision/controllers"
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

//SetCentrosRouters router for centros endpoints
func SetCentrosRouters(router *mux.Router) *mux.Router {
	centrosRouter := mux.NewRouter()
	centrosRouter.HandleFunc("/api/centros", controllers.Centros.Create).Methods("POST")
	centrosRouter.HandleFunc("/api/centros", controllers.Centros.Get).Methods("GET")
	centrosRouter.HandleFunc("/api/centros/{id}", controllers.Centros.GetOne).Methods("GET")
	centrosRouter.HandleFunc("/api/centros/{id}", controllers.Centros.Update).Methods("PUT")
	centrosRouter.HandleFunc("/api/centros/{id}", controllers.Centros.Delete).Methods("DELETE")
	router.PathPrefix("/api/centros").Handler(
		negroni.New(
			negroni.HandlerFunc(common.Authorize),
			negroni.Wrap(centrosRouter),
		))
	return router
}
