package routers

import (
	"bitbucket.org/colombia9503/AgriculturaDePrecision/common"
	"bitbucket.org/colombia9503/AgriculturaDePrecision/controllers"
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

//SetGraphResultadosRouter router for detalleResultados endpoints
func SetGraphResultadosRouter(router *mux.Router) *mux.Router {
	graphResultadosRouter := mux.NewRouter()
	graphResultadosRouter.HandleFunc("/graph/resultados/{id}", controllers.Resultados.GraphResultado).Methods("GET")
	router.PathPrefix("/graph/resultados").Handler(
		negroni.New(
			negroni.HandlerFunc(common.Authorize),
			negroni.Wrap(graphResultadosRouter),
		))
	return router
}
