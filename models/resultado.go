package models

import (
	"bitbucket.org/colombia9503/AgriculturaDePrecision/common"
	chart "github.com/wcharczuk/go-chart"
)

//Resultado struct
type Resultado struct {
	ID     int    `db:"id"`
	Nombre string `db:"nombre"`
}

type resultados struct{}

//Usuarios func collection to be exported
var Resultados = new(resultados)

func (resultados) SelectAll() ([]Resultado, error) {
	resultados := []Resultado{}
	rows, err := common.DB.Query("SELECT * FROM resultados;")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var resultado Resultado
		if err := rows.Scan(&resultado.ID, &resultado.Nombre); err != nil {
			return nil, err
		}
		resultados = append(resultados, resultado)
	}
	return resultados, nil
}

func (resultados) SelectOne(resultado Resultado) (Resultado, error) {
	row := common.DB.QueryRow("SELECT * FROM resultados WHERE id = ?;", resultado.ID)
	if err := row.Scan(&resultado.ID, &resultado.Nombre); err != nil {
		return resultado, err
	}
	return resultado, nil
}

func (resultados) Insert(resultado Resultado) error {
	stmt, err := common.DB.Prepare("INSERT INTO resultados (nombre) VALUES (?);")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(&resultado.Nombre)
	defer stmt.Close()
	if err != nil {
		return err
	}
	return nil
}

func (resultados) Update(resultado Resultado) error {
	stmt, err := common.DB.Prepare("UPDATE resultados SET nombre = COALESCE(?, nombre) WHERE id = ?;")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(resultado.Nombre, resultado.ID)
	defer stmt.Close()
	return err
}

func (resultados) Delete(resultado Resultado) error {
	stmt, err := common.DB.Prepare("DELETE FROM resultados WHERE id = ?;")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(resultado.ID)
	defer stmt.Close()
	return err
}

func (resultados) GenerateVectorValues(resultado Resultado) ([]chart.Value, error) {
	vectors := []chart.Value{}
	rows, err := common.DB.Query("SELECT d.valor, p.nombre FROM detalle_resultado d, tipopruebas p WHERE d.tipo_prueba = p.id AND resultado = ?;", resultado.ID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var vector chart.Value
		if err := rows.Scan(&vector.Value, &vector.Label); err != nil {
			return nil, err
		}
		vectors = append(vectors, vector)
	}
	return vectors, nil
}
