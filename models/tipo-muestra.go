package models

import "bitbucket.org/colombia9503/AgriculturaDePrecision/common"

//TipoMuestra struct
type TipoMuestra struct {
	ID     int    `db:"id"`
	Nombre string `db:"nombre"`
}

type tipoMuestras struct{}

//TipoMuestras func collection to be exported
var TipoMuestras = new(tipoMuestras)

func (tipoMuestras) SelectAll() ([]TipoMuestra, error) {
	tipoMuestras := []TipoMuestra{}
	rows, err := common.DB.Query("SELECT * FROM tipoMuestras;")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var tipoMuestra TipoMuestra
		if err := rows.Scan(&tipoMuestra.ID, &tipoMuestra.Nombre); err != nil {
			return nil, err
		}
		tipoMuestras = append(tipoMuestras, tipoMuestra)
	}
	return tipoMuestras, nil
}

func (tipoMuestras) SelectOne(tipoMuestra TipoMuestra) (TipoMuestra, error) {
	row := common.DB.QueryRow("SELECT * FROM tipomuestras WHERE id = ?;", tipoMuestra.ID)
	if err := row.Scan(&tipoMuestra.ID, &tipoMuestra.Nombre); err != nil {
		return tipoMuestra, err
	}
	return tipoMuestra, nil
}

func (tipoMuestras) Insert(tipoMuestra TipoMuestra) error {
	stmt, err := common.DB.Prepare("INSERT INTO tipomuestras (nombre) VALUES (?);")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(&tipoMuestra.Nombre)
	defer stmt.Close()
	if err != nil {
		return err
	}
	return nil
}

func (tipoMuestras) Update(tipoMuestra TipoMuestra) error {
	stmt, err := common.DB.Prepare("UPDATE tipomuestras SET nombre = COALESCE(?, nombre) WHERE id = ?;")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(tipoMuestra.Nombre, tipoMuestra.ID)
	defer stmt.Close()
	return err
}

func (tipoMuestras) Delete(tipoMuestra TipoMuestra) error {
	stmt, err := common.DB.Prepare("DELETE FROM tipomuestras WHERE id = ?;")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(tipoMuestra.ID)
	defer stmt.Close()
	return err
}
