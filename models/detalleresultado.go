package models

import "bitbucket.org/colombia9503/AgriculturaDePrecision/common"

//DetalleResultado struct
type DetalleResultado struct {
	ID         int        `db:"id"`
	Resultado  Resultado  `db:"resultado"`
	Muestra    Muestra    `db:"muestra"`
	TipoPrueba TipoPrueba `db:"tipo_prueba"`
	Valor      float64    `db:"valor"`
}

type detalleResultados struct{}

//Procesamientos func collection to be exported
var DetalleResultados = new(detalleResultados)

func (detalleResultados) SelectAll() ([]DetalleResultado, error) {
	detalleResultados := []DetalleResultado{}
	rows, err := common.DB.Query("SELECT d.id, r.id, r.nombre, m.id, m.tipo_muestra_id, m.medida, " +
		"u.id, u.usuario, u.nombre, c.id, c.nombre, u.rol, " +
		"p.id, p.nombre, d.valor " +
		"FROM detalle_resultado d, resultados r, muestras m, tipopruebas p, usuarios u, centros c " +
		"WHERE d.resultado = r.id AND m.id = d.muestra AND d.tipo_prueba = p.id " +
		"AND u.centro = c.id;")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var detalleResultado DetalleResultado
		if err := rows.Scan(&detalleResultado.ID, &detalleResultado.Resultado.ID, &detalleResultado.Resultado.Nombre, &detalleResultado.Muestra.ID,
			&detalleResultado.Muestra.TipoMuestra.ID, &detalleResultado.Muestra.Medida, &detalleResultado.Muestra.Usuario.ID, &detalleResultado.Muestra.Usuario.Usuario,
			&detalleResultado.Muestra.Usuario.Nombre, &detalleResultado.Muestra.Usuario.Centro.ID, &detalleResultado.Muestra.Usuario.Centro.Nombre,
			&detalleResultado.Muestra.Usuario.Rol, &detalleResultado.TipoPrueba.ID,
			&detalleResultado.TipoPrueba.Nombre, &detalleResultado.Valor); err != nil {
			return nil, err
		}
		detalleResultados = append(detalleResultados, detalleResultado)
	}
	return detalleResultados, nil
}

func (detalleResultados) SelectOne(detalleResultado DetalleResultado) (DetalleResultado, error) {
	row := common.DB.QueryRow("SELECT r.id, r.nombre, m.id, m.tipo_muestra_id, m.medida, "+
		"u.id, u.usuario, u.nombre, c.id, c.nombre, u.rol, "+
		"p.id, p.nombre, d.valor "+
		"FROM detalle_resultado d, resultados r, muestras m, tipopruebas p, usuarios u, centros c "+
		"WHERE d.resultado = r.id AND m.id = d.muestra AND d.tipo_prueba = p.id "+
		"AND u.centro = c.id AND d.id = ?;", detalleResultado.ID)
	if err := row.Scan(&detalleResultado.Resultado.ID, &detalleResultado.Resultado.Nombre, &detalleResultado.Muestra.ID,
		&detalleResultado.Muestra.TipoMuestra.ID, &detalleResultado.Muestra.Medida, &detalleResultado.Muestra.Usuario.ID, &detalleResultado.Muestra.Usuario.Usuario,
		&detalleResultado.Muestra.Usuario.Nombre, &detalleResultado.Muestra.Usuario.Centro.ID, &detalleResultado.Muestra.Usuario.Centro.Nombre,
		&detalleResultado.Muestra.Usuario.Rol, &detalleResultado.TipoPrueba.ID,
		&detalleResultado.TipoPrueba.Nombre, &detalleResultado.Valor, &detalleResultado.ID); err != nil {
		return detalleResultado, err
	}
	return detalleResultado, nil
}

func (detalleResultados) Insert(detalleResultado DetalleResultado) error {
	stmt, err := common.DB.Prepare("INSERT INTO detalle_resultado (resultado, muestra, tipo_prueba, valor) VALUES (?, ?, ?, ?);")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(detalleResultado.Resultado.ID, detalleResultado.Muestra.ID, detalleResultado.TipoPrueba.ID, detalleResultado.Valor)
	defer stmt.Close()
	if err != nil {
		return err
	}
	return nil
}

func (detalleResultados) Update(detalleResultado DetalleResultado) error {
	stmt, err := common.DB.Prepare("UPDATE detalle_resultado SET resultado = COALESCE(?, resultado), muestra = COALESCE(?, muestra), tipo_prueba = COALESCE(?, tipo_prueba), valor = COALESCE(?, valor) WHERE id = ?;")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(detalleResultado.Resultado.ID, detalleResultado.Muestra.ID, detalleResultado.TipoPrueba.ID, detalleResultado.Valor, detalleResultado.ID)
	defer stmt.Close()
	return err
}

func (detalleResultados) Delete(detalleResultado DetalleResultado) error {
	stmt, err := common.DB.Prepare("DELETE FROM detalle_resultado WHERE id = ?;")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(detalleResultado.ID)
	defer stmt.Close()
	return err
}
