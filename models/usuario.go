package models

import (
	"crypto/rand"
	"crypto/sha512"
	"encoding/hex"

	"bitbucket.org/colombia9503/AgriculturaDePrecision/common"
)

//Usuario model structure
type Usuario struct {
	ID       int    `db:"id" json:"ID"`
	Usuario  string `db:"usuario" json:"Usuario"`
	Nombre   string `db:"nombre" json:"Nombre"`
	Password string `db:"password" json:"Password"`
	Salt     string `db:"salt" json:"Salt"`
	Centro   Centro `db:"centro" json:"Centro"`
	Rol      int    `db:"rol" json:"Rol"`
}

type usuarios struct{}

//Usuarios func collection to be exported
var Usuarios = new(usuarios)

func (usuarios) SelectAll() ([]Usuario, error) {
	usuarios := []Usuario{}
	rows, err := common.DB.Query("SELECT u.id, usuario, u.nombre, c.id, c.nombre as centro, rol FROM usuarios u, centros c WHERE u.centro = c.id;")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var usuario Usuario
		var centro Centro
		if err := rows.Scan(&usuario.ID, &usuario.Usuario, &usuario.Nombre, &centro.ID, &centro.Nombre, &usuario.Rol); err != nil {
			return nil, err
		}
		usuario.Centro = centro
		usuarios = append(usuarios, usuario)
	}
	return usuarios, nil
}

func (usuarios) SelectOne(usuario Usuario) (Usuario, error) {
	var centro Centro
	row := common.DB.QueryRow("SELECT u.id, usuario, u.nombre, c.id, c.nombre as centro, rol FROM usuarios u, centros c WHERE u.id = ? AND u.centro = c.id;", usuario.ID)
	if err := row.Scan(&usuario.ID, &usuario.Usuario, &usuario.Nombre, &centro.ID, &centro.Nombre, &usuario.Rol); err != nil {
		return usuario, err
	}
	usuario.Centro = centro
	return usuario, nil
}

func (usuarios) Insert(usuario Usuario) error {
	stmt, err := common.DB.Prepare("INSERT INTO usuarios(usuario, nombre, password, salt, centro, rol) VALUES(?, ?, ?, ?, ?, ?);")
	if err != nil {
		return err
	}

	if usuario.Salt, err = generateSalt(); err != nil {
		return err
	}

	if usuario.Password, err = generateEncryptedPassword(usuario.Password, usuario.Salt); err != nil {
		return err
	}

	_, err = stmt.Exec(&usuario.Usuario, &usuario.Nombre, &usuario.Password, &usuario.Salt, &usuario.Centro.ID, &usuario.Rol)
	defer stmt.Close()
	if err != nil {
		return err
	}
	return nil
}

func (usuarios) Update(usuario Usuario) error {
	stmt, err := common.DB.Prepare("UPDATE usuarios SET usuario = COALESCE(?, usuario), nombre = COALESCE(?, nombre), centro = COALESCE(?, centro), rol = COALESCE(?, rol) WHERE id = ?;")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(&usuario.Usuario, &usuario.Nombre, &usuario.Centro.ID, &usuario.Rol, &usuario.ID)
	defer stmt.Close()
	return err
}

func (usuarios) Delete(usuario Usuario) error {
	stmt, err := common.DB.Prepare("DELETE FROM usuarios WHERE id = ?;")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(&usuario.ID)
	defer stmt.Close()
	return err
}

func generateRandombytes() ([]byte, error) {
	b := make([]byte, 64)
	if _, err := rand.Read(b); err != nil {
		return nil, err
	}
	return b, nil
}

func generateSalt() (string, error) {
	salt, err := generateRandombytes()
	return hex.EncodeToString(salt), err
}

func generateEncryptedPassword(password, salt string) (string, error) {
	h512 := sha512.New()
	h512.Write([]byte(password))
	pw := hex.EncodeToString(h512.Sum(nil)[:]) + salt
	h512pw := sha512.New()
	h512pw.Write([]byte(pw))
	return hex.EncodeToString(h512pw.Sum(nil)[:]), nil
}
