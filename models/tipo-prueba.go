package models

import "bitbucket.org/colombia9503/AgriculturaDePrecision/common"

//TipoPrueba struct
type TipoPrueba struct {
	ID     int    `db:"id"`
	Nombre string `db:"nombre"`
}

type tipoPruebas struct{}

//TipoPruebas func collection to be exported
var TipoPruebas = new(tipoPruebas)

func (tipoPruebas) SelectAll() ([]TipoPrueba, error) {
	tipoPruebas := []TipoPrueba{}
	rows, err := common.DB.Query("SELECT * FROM tipopruebas;")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var tipoPrueba TipoPrueba
		if err := rows.Scan(&tipoPrueba.ID, &tipoPrueba.Nombre); err != nil {
			return nil, err
		}
		tipoPruebas = append(tipoPruebas, tipoPrueba)
	}
	return tipoPruebas, nil
}

func (tipoPruebas) SelectOne(tipoPrueba TipoPrueba) (TipoPrueba, error) {
	row := common.DB.QueryRow("SELECT * FROM tipopruebas WHERE id = ?;", tipoPrueba.ID)
	if err := row.Scan(&tipoPrueba.ID, &tipoPrueba.Nombre); err != nil {
		return tipoPrueba, err
	}
	return tipoPrueba, nil
}

func (tipoPruebas) Insert(tipoPrueba TipoPrueba) error {
	stmt, err := common.DB.Prepare("INSERT INTO tipopruebas (nombre) VALUES (?);")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(&tipoPrueba.Nombre)
	defer stmt.Close()
	if err != nil {
		return err
	}
	return nil
}

func (tipoPruebas) Update(tipoPrueba TipoPrueba) error {
	stmt, err := common.DB.Prepare("UPDATE tipopruebas SET nombre = COALESCE(?, nombre) WHERE id = ?;")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(tipoPrueba.Nombre, tipoPrueba.ID)
	defer stmt.Close()
	return err
}

func (tipoPruebas) Delete(tipoPrueba TipoPrueba) error {
	stmt, err := common.DB.Prepare("DELETE FROM tipopruebas WHERE id = ?;")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(tipoPrueba.ID)
	defer stmt.Close()
	return err
}
