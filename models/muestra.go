package models

import "bitbucket.org/colombia9503/AgriculturaDePrecision/common"

//Muestra struct
type Muestra struct {
	ID          int         `db:"id"`
	TipoMuestra TipoMuestra `db:"tipo_muestra_id"`
	Medida      float64     `db:"medida"`
	Usuario     Usuario     `db:"usuario"`
}

type muestras struct{}

//TipoMuestras func collection to be exported
var Muestras = new(muestras)

func (muestras) SelectAll() ([]Muestra, error) {
	muestras := []Muestra{}
	rows, err := common.DB.Query("SELECT m.id, t.id, t.nombre, medida, u.id, u.usuario, u.nombre, c.id, c.nombre, u.rol " +
		"FROM muestras m, tipomuestras t, usuarios u, centros c WHERE m.tipo_muestra_id = t.id AND m.usuario = u.id AND u.centro = c.id;")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var muestra Muestra
		if err := rows.Scan(&muestra.ID, &muestra.TipoMuestra.ID, &muestra.TipoMuestra.Nombre, &muestra.Medida, &muestra.Usuario.ID, &muestra.Usuario.Usuario,
			&muestra.Usuario.Nombre, &muestra.Usuario.Centro.ID, &muestra.Usuario.Centro.Nombre, &muestra.Usuario.Rol); err != nil {
			return nil, err
		}
		muestras = append(muestras, muestra)
	}
	return muestras, nil
}

func (muestras) SelectOne(muestra Muestra) (Muestra, error) {
	row := common.DB.QueryRow("SELECT t.id, t.nombre, medida, u.id, u.usuario, u.nombre, c.id, c.nombre, u.rol "+
		"FROM muestras m, tipomuestras t, usuarios u, centros c "+
		"WHERE m.tipo_muestra_id = t.id AND m.usuario = u.id "+
		"AND u.centro = c.id; AND m.id = ?", muestra.ID)
	if err := row.Scan(&muestra.TipoMuestra.ID, &muestra.TipoMuestra.Nombre, &muestra.Medida, &muestra.Usuario.ID, &muestra.Usuario.Usuario,
		&muestra.Usuario.Nombre, &muestra.Usuario.Centro.ID, &muestra.Usuario.Centro.Nombre, &muestra.Usuario.Rol); err != nil {
		return muestra, err
	}
	return muestra, nil
}

func (muestras) Insert(muestra Muestra) error {
	stmt, err := common.DB.Prepare("INSERT INTO muestras (tipo_muestra_id, medida, usuario) VALUES (?, ?, ?);")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(muestra.TipoMuestra.ID, muestra.Medida, muestra.Usuario.ID)
	defer stmt.Close()
	if err != nil {
		return err
	}
	return nil
}

func (muestras) Update(muestra Muestra) error {
	stmt, err := common.DB.Prepare("UPDATE muestras SET tipo_muestra_id = COALESCE(?, tipo_muestra_id), medida = COALESCE(?, medida), usuario = COALESCE(?, usuario) WHERE id = ?;")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(muestra.TipoMuestra.ID, muestra.Medida, muestra.Usuario.ID, muestra.ID)
	defer stmt.Close()
	return err
}

func (muestras) Delete(muestra Muestra) error {
	stmt, err := common.DB.Prepare("DELETE FROM muestras WHERE id = ?;")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(muestra.ID)
	defer stmt.Close()
	return err
}
