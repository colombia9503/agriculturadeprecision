package models

import (
	"crypto/sha512"
	"encoding/hex"
	"fmt"
	"net/http"

	"bitbucket.org/colombia9503/AgriculturaDePrecision/common"
)

type Credencial struct {
	Token string `json:"Token"`
}

var Auth = new(auth)

type auth struct{}

func (auth) Login(usuario Usuario) (*Credencial, error) {
	var usuarioActual Usuario
	row := common.DB.QueryRow("SELECT usuario, u.nombre, c.id, c.nombre,  rol, password, salt FROM usuarios u, centros c WHERE usuario LIKE ?", usuario.Usuario)
	fmt.Println(usuario)
	if err := row.Scan(&usuarioActual.Usuario, &usuarioActual.Nombre, &usuarioActual.Centro.ID, &usuarioActual.Centro.Nombre, &usuarioActual.Rol, &usuarioActual.Password, &usuarioActual.Salt); err != nil {
		//err = no sql rows
		return nil, err
	}

	if ComparePasswords(usuario.Password, usuarioActual.Password, usuarioActual.Salt) {
		token, err := common.GenerateJWT(usuarioActual.Usuario, usuarioActual.Nombre, usuarioActual.Rol, usuarioActual.Centro.ID)
		if err != nil {
			return nil, err
		}
		return &Credencial{Token: token}, nil
	}

	return nil, common.NewLogErr("Invalid Login Credentials")
}

func (auth) Logout(r *http.Request) error {
	return nil
}

func ComparePasswords(pwd, bdpwd, salt string) bool {
	h512 := sha512.New()
	h512.Write([]byte(pwd))
	pw := hex.EncodeToString(h512.Sum(nil)[:]) + salt
	h512pw := sha512.New()
	h512pw.Write([]byte(pw))
	fmt.Println(hex.EncodeToString(h512pw.Sum(nil)[:]))
	if hex.EncodeToString(h512pw.Sum(nil)[:]) == bdpwd {
		return true
	}
	return false
}
