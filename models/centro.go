package models

import "bitbucket.org/colombia9503/AgriculturaDePrecision/common"

//Centro model structure
type Centro struct {
	ID     int    `db:"id" json:"ID"`
	Nombre string `db:"nombre" json:"Nombre" required:"true" description:"Nombre del centro"`
}

type centros struct{}

//Centros func collection to be exported
var Centros = new(centros)

func (centros) SelectAll() ([]Centro, error) {
	centros := []Centro{}
	rows, err := common.DB.Query("SELECT * FROM centros;")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var centro Centro
		if err := rows.Scan(&centro.ID, &centro.Nombre); err != nil {
			return nil, err
		}
		centros = append(centros, centro)
	}
	return centros, nil
}

func (centros) SelectOne(centro Centro) (Centro, error) {
	row := common.DB.QueryRow("SELECT * FROM centros WHERE id = ?;", centro.ID)
	if err := row.Scan(&centro.ID, &centro.Nombre); err != nil {
		return centro, err
	}
	return centro, nil
}

func (centros) Insert(centro Centro) error {
	stmt, err := common.DB.Prepare("INSERT INTO centros (nombre) VALUES (?);")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(&centro.Nombre)
	defer stmt.Close()
	if err != nil {
		return err
	}
	return nil
}

func (centros) Update(centro Centro) error {
	stmt, err := common.DB.Prepare("UPDATE centros SET nombre = COALESCE(?, nombre) WHERE id = ?;")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(centro.Nombre, centro.ID)
	defer stmt.Close()
	return err
}

func (centros) Delete(centro Centro) error {
	stmt, err := common.DB.Prepare("DELETE FROM centros WHERE id = ?;")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(centro.ID)
	defer stmt.Close()
	return err
}
