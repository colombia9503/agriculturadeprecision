package main

/**
 * GOPHER: Santi ʕ◔ϖ◔ʔ
 */

import (
	"log"
	"net/http"

	"bitbucket.org/colombia9503/AgriculturaDePrecision/common"
	"bitbucket.org/colombia9503/AgriculturaDePrecision/routers"
	"github.com/urfave/negroni"
)

func main() {
	common.StartUp()
	r := routers.InitRouters()

	n := negroni.Classic()
	n.UseHandler(r)
	log.Println("Listening.. 9999")
	http.ListenAndServe(":9999", n)
}
