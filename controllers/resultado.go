package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"bitbucket.org/colombia9503/AgriculturaDePrecision/common"
	"bitbucket.org/colombia9503/AgriculturaDePrecision/models"
	"github.com/gorilla/mux"
	chart "github.com/wcharczuk/go-chart"
)

type resultadoController struct{}

//Centros exports the func collection
var Resultados = new(resultadoController)

func (rc *resultadoController) Create(w http.ResponseWriter, r *http.Request) {
	var resultado models.Resultado
	if err := json.NewDecoder(r.Body).Decode(&resultado); err != nil {
		common.JsonError(w, err, http.StatusNotAcceptable)
		return
	}
	err := models.Resultados.Insert(resultado)
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}
	common.JsonStatus(w, http.StatusOK)
}

func (rc *resultadoController) Get(w http.ResponseWriter, r *http.Request) {
	resultados, err := models.Resultados.SelectAll()
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	res, err := json.Marshal(resultados)
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	common.JsonOk(w, res, http.StatusOK)
}

func (rc *resultadoController) GetOne(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	var resultado models.Resultado
	resultado.ID = id
	result, err := models.Resultados.SelectOne(resultado)
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	res, err := json.Marshal(result)
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	common.JsonOk(w, res, http.StatusOK)
}

func (rc *resultadoController) Update(w http.ResponseWriter, r *http.Request) {
	var resultado models.Resultado
	vars := mux.Vars(r)

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	if err := json.NewDecoder(r.Body).Decode(&resultado); err != nil {
		common.JsonError(w, err, http.StatusNotAcceptable)
		return
	}

	resultado.ID = id
	err = models.Resultados.Update(resultado)
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	common.JsonStatus(w, http.StatusOK)
}

func (rc *resultadoController) Delete(w http.ResponseWriter, r *http.Request) {
	var resultado models.Resultado
	vars := mux.Vars(r)

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	resultado.ID = id
	err = models.Resultados.Delete(resultado)
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	common.JsonStatus(w, http.StatusOK)
}

func (rc *resultadoController) GraphResultado(w http.ResponseWriter, r *http.Request) {
	var resultado models.Resultado
	vars := mux.Vars(r)

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	resultado.ID = id
	vectors, err := models.Resultados.GenerateVectorValues(resultado)
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}
	fmt.Println(vectors)

	sbc := chart.BarChart{
		Height:   512,
		BarWidth: 60,
		XAxis: chart.Style{
			Show: true,
		},
		YAxis: chart.YAxis{
			Style: chart.Style{
				Show: true,
			},
		},
		Bars: vectors,
	}

	w.Header().Set("Content-Type", "image/png")
	err = sbc.Render(chart.PNG, w)
	if err != nil {
		common.JsonError(w, err, http.StatusConflict)
		return
	}
	common.JsonStatus(w, http.StatusOK)
}
