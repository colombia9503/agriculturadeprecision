package controllers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"bitbucket.org/colombia9503/AgriculturaDePrecision/common"
	"bitbucket.org/colombia9503/AgriculturaDePrecision/models"
	"github.com/gorilla/mux"
)

type centrosController struct{}

//Centros exports the func collection
var Centros = new(centrosController)

// @Title CrearNuevoCentro
// @Description retrieves orders for given customer defined by customer ID
// @Accept  json
// @Success 200 {array}  AgriculturaDePresicion.model.Centro
// @Failure 400 {object} AgriculturaDePresicion..common.JsonStatus    "Ocurrio un error guardando"
// @Resource /centro
// @Router /api/centros [post]
func (cc *centrosController) Create(w http.ResponseWriter, r *http.Request) {
	var centro models.Centro
	if err := json.NewDecoder(r.Body).Decode(&centro); err != nil {
		common.JsonError(w, err, http.StatusNotAcceptable)
		return
	}
	err := models.Centros.Insert(centro)
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}
	common.JsonStatus(w, http.StatusOK)
}

func (cc *centrosController) Get(w http.ResponseWriter, r *http.Request) {
	centros, err := models.Centros.SelectAll()
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	res, err := json.Marshal(centros)
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	common.JsonOk(w, res, http.StatusOK)
}

func (cc *centrosController) GetOne(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	var centro models.Centro
	centro.ID = id
	result, err := models.Centros.SelectOne(centro)
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	res, err := json.Marshal(result)
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	common.JsonOk(w, res, http.StatusOK)
}

func (cc *centrosController) Update(w http.ResponseWriter, r *http.Request) {
	var centro models.Centro
	vars := mux.Vars(r)

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	if err := json.NewDecoder(r.Body).Decode(&centro); err != nil {
		common.JsonError(w, err, http.StatusNotAcceptable)
		return
	}

	centro.ID = id
	err = models.Centros.Update(centro)
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	common.JsonStatus(w, http.StatusOK)
}

func (cc *centrosController) Delete(w http.ResponseWriter, r *http.Request) {
	var centro models.Centro
	vars := mux.Vars(r)

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	centro.ID = id
	err = models.Centros.Delete(centro)
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	common.JsonStatus(w, http.StatusOK)
}
