package controllers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"bitbucket.org/colombia9503/AgriculturaDePrecision/common"
	"bitbucket.org/colombia9503/AgriculturaDePrecision/models"
	"github.com/gorilla/mux"
)

type tipoPruebasController struct{}

//TipoPruebas exports the func collection
var TipoPruebas = new(tipoPruebasController)

func (tpc *tipoPruebasController) Create(w http.ResponseWriter, r *http.Request) {
	var tipoPruebas models.TipoPrueba
	if err := json.NewDecoder(r.Body).Decode(&tipoPruebas); err != nil {
		common.JsonError(w, err, http.StatusNotAcceptable)
		return
	}
	err := models.TipoPruebas.Insert(tipoPruebas)
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}
	common.JsonStatus(w, http.StatusOK)
}

func (tpc *tipoPruebasController) Get(w http.ResponseWriter, r *http.Request) {
	tipoPruebas, err := models.TipoPruebas.SelectAll()
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	res, err := json.Marshal(tipoPruebas)
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	common.JsonOk(w, res, http.StatusOK)
}

func (tpc *tipoPruebasController) GetOne(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	var tipoPrueba models.TipoPrueba
	tipoPrueba.ID = id
	result, err := models.TipoPruebas.SelectOne(tipoPrueba)
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	res, err := json.Marshal(result)
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	common.JsonOk(w, res, http.StatusOK)
}

func (tpc *tipoPruebasController) Update(w http.ResponseWriter, r *http.Request) {
	var tipoPrueba models.TipoPrueba
	vars := mux.Vars(r)

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	if err := json.NewDecoder(r.Body).Decode(&tipoPrueba); err != nil {
		common.JsonError(w, err, http.StatusNotAcceptable)
		return
	}

	tipoPrueba.ID = id
	err = models.TipoPruebas.Update(tipoPrueba)
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	common.JsonStatus(w, http.StatusOK)
}

func (tpc *tipoPruebasController) Delete(w http.ResponseWriter, r *http.Request) {
	var tipoPrueba models.TipoPrueba
	vars := mux.Vars(r)

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	tipoPrueba.ID = id
	err = models.TipoPruebas.Delete(tipoPrueba)
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	common.JsonStatus(w, http.StatusOK)
}
