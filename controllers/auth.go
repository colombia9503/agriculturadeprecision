package controllers

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/colombia9503/AgriculturaDePrecision/common"
	"bitbucket.org/colombia9503/AgriculturaDePrecision/models"
)

type authController struct{}

var Auth = new(authController)

func (ac *authController) Login(w http.ResponseWriter, r *http.Request) {
	var usuario models.Usuario

	if err := json.NewDecoder(r.Body).Decode(&usuario); err != nil {
		common.JsonError(w, err, http.StatusNotAcceptable)
		return
	}

	result, err := models.Auth.Login(usuario)
	if err != nil {
		common.JsonError(w, err, http.StatusUnauthorized)
		return
	}

	res, err := json.Marshal(result)
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	common.JsonOk(w, res, http.StatusOK)
}
