package controllers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"bitbucket.org/colombia9503/AgriculturaDePrecision/common"
	"bitbucket.org/colombia9503/AgriculturaDePrecision/models"
	"github.com/gorilla/mux"
)

type usuariosController struct{}

//Usuarios exports the func collection
var Usuarios = new(usuariosController)

func (uc *usuariosController) Create(w http.ResponseWriter, r *http.Request) {
	var usuario models.Usuario
	if err := json.NewDecoder(r.Body).Decode(&usuario); err != nil {
		common.JsonError(w, err, http.StatusNotAcceptable)
		return
	}
	err := models.Usuarios.Insert(usuario)
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}
	common.JsonStatus(w, http.StatusOK)
}

func (uc *usuariosController) Get(w http.ResponseWriter, r *http.Request) {
	usuarios, err := models.Usuarios.SelectAll()
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	res, err := json.Marshal(usuarios)
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	common.JsonOk(w, res, http.StatusOK)
}

func (uc *usuariosController) GetOne(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	var usuario models.Usuario
	usuario.ID = id
	result, err := models.Usuarios.SelectOne(usuario)
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	res, err := json.Marshal(result)
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	common.JsonOk(w, res, http.StatusOK)
}

func (uc *usuariosController) Update(w http.ResponseWriter, r *http.Request) {
	var usuario models.Usuario
	vars := mux.Vars(r)

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	if err := json.NewDecoder(r.Body).Decode(&usuario); err != nil {
		common.JsonError(w, err, http.StatusNotAcceptable)
		return
	}

	usuario.ID = id
	err = models.Usuarios.Update(usuario)
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	common.JsonStatus(w, http.StatusOK)
}

func (uc *usuariosController) Delete(w http.ResponseWriter, r *http.Request) {
	var usuario models.Usuario
	vars := mux.Vars(r)

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	usuario.ID = id
	err = models.Usuarios.Delete(usuario)
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	common.JsonStatus(w, http.StatusOK)
}
