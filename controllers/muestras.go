package controllers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"bitbucket.org/colombia9503/AgriculturaDePrecision/common"
	"bitbucket.org/colombia9503/AgriculturaDePrecision/models"
	"github.com/gorilla/mux"
)

type muestrasController struct{}

//Muestras exports the func collection
var Muestras = new(muestrasController)

func (mc *muestrasController) Create(w http.ResponseWriter, r *http.Request) {
	var muestra models.Muestra
	if err := json.NewDecoder(r.Body).Decode(&muestra); err != nil {
		common.JsonError(w, err, http.StatusNotAcceptable)
		return
	}
	err := models.Muestras.Insert(muestra)
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}
	common.JsonStatus(w, http.StatusOK)
}

func (mc *muestrasController) Get(w http.ResponseWriter, r *http.Request) {
	muestras, err := models.Muestras.SelectAll()
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	res, err := json.Marshal(muestras)
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	common.JsonOk(w, res, http.StatusOK)
}

func (mc *muestrasController) GetOne(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	var muestra models.Muestra
	muestra.ID = id
	result, err := models.Muestras.SelectOne(muestra)
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	res, err := json.Marshal(result)
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	common.JsonOk(w, res, http.StatusOK)
}

func (mc *muestrasController) Update(w http.ResponseWriter, r *http.Request) {
	var muestra models.Muestra
	vars := mux.Vars(r)

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	if err := json.NewDecoder(r.Body).Decode(&muestra); err != nil {
		common.JsonError(w, err, http.StatusNotAcceptable)
		return
	}

	muestra.ID = id
	err = models.Muestras.Update(muestra)
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	common.JsonStatus(w, http.StatusOK)
}

func (mc *muestrasController) Delete(w http.ResponseWriter, r *http.Request) {
	var muestra models.Muestra
	vars := mux.Vars(r)

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	muestra.ID = id
	err = models.Muestras.Delete(muestra)
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	common.JsonStatus(w, http.StatusOK)
}
