package controllers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"bitbucket.org/colombia9503/AgriculturaDePrecision/common"
	"bitbucket.org/colombia9503/AgriculturaDePrecision/models"
	"github.com/gorilla/mux"
)

type detalleResultadoController struct{}

//DetalleResultados exports the func collection
var DetalleResultados = new(detalleResultadoController)

func (drc *detalleResultadoController) Create(w http.ResponseWriter, r *http.Request) {
	var detalleResultado models.DetalleResultado
	if err := json.NewDecoder(r.Body).Decode(&detalleResultado); err != nil {
		common.JsonError(w, err, http.StatusNotAcceptable)
		return
	}
	err := models.DetalleResultados.Insert(detalleResultado)
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}
	common.JsonStatus(w, http.StatusOK)
}

func (drc *detalleResultadoController) Get(w http.ResponseWriter, r *http.Request) {
	detalleResultados, err := models.DetalleResultados.SelectAll()
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	res, err := json.Marshal(detalleResultados)
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	common.JsonOk(w, res, http.StatusOK)
}

func (drc *detalleResultadoController) GetOne(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	var detalleResultado models.DetalleResultado
	detalleResultado.ID = id
	result, err := models.DetalleResultados.SelectOne(detalleResultado)
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	res, err := json.Marshal(result)
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	common.JsonOk(w, res, http.StatusOK)
}

func (drc *detalleResultadoController) Update(w http.ResponseWriter, r *http.Request) {
	var detalleResultado models.DetalleResultado
	vars := mux.Vars(r)

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	if err := json.NewDecoder(r.Body).Decode(&detalleResultado); err != nil {
		common.JsonError(w, err, http.StatusNotAcceptable)
		return
	}

	detalleResultado.ID = id
	err = models.DetalleResultados.Update(detalleResultado)
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	common.JsonStatus(w, http.StatusOK)
}

func (drc *detalleResultadoController) Delete(w http.ResponseWriter, r *http.Request) {
	var detalleResultado models.DetalleResultado
	vars := mux.Vars(r)

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	detalleResultado.ID = id
	err = models.DetalleResultados.Delete(detalleResultado)
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	common.JsonStatus(w, http.StatusOK)
}
