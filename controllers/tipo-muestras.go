package controllers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"bitbucket.org/colombia9503/AgriculturaDePrecision/common"
	"bitbucket.org/colombia9503/AgriculturaDePrecision/models"
	"github.com/gorilla/mux"
)

type tipoMuestrasController struct{}

//TipoMuestras exports the func collection
var TipoMuestras = new(tipoMuestrasController)

func (tmc *tipoMuestrasController) Create(w http.ResponseWriter, r *http.Request) {
	var tipoMuestra models.TipoMuestra
	if err := json.NewDecoder(r.Body).Decode(&tipoMuestra); err != nil {
		common.JsonError(w, err, http.StatusNotAcceptable)
		return
	}
	err := models.TipoMuestras.Insert(tipoMuestra)
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}
	common.JsonStatus(w, http.StatusOK)
}

func (tmc *tipoMuestrasController) Get(w http.ResponseWriter, r *http.Request) {
	tipoMuestras, err := models.TipoMuestras.SelectAll()
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	res, err := json.Marshal(tipoMuestras)
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	common.JsonOk(w, res, http.StatusOK)
}

func (tmc *tipoMuestrasController) GetOne(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	var tipoMuestra models.TipoMuestra
	tipoMuestra.ID = id
	result, err := models.TipoMuestras.SelectOne(tipoMuestra)
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	res, err := json.Marshal(result)
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	common.JsonOk(w, res, http.StatusOK)
}

func (tmc *tipoMuestrasController) Update(w http.ResponseWriter, r *http.Request) {
	var tipoMuestra models.TipoMuestra
	vars := mux.Vars(r)

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	if err := json.NewDecoder(r.Body).Decode(&tipoMuestra); err != nil {
		common.JsonError(w, err, http.StatusNotAcceptable)
		return
	}

	tipoMuestra.ID = id
	err = models.TipoMuestras.Update(tipoMuestra)
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	common.JsonStatus(w, http.StatusOK)
}

func (tmc *tipoMuestrasController) Delete(w http.ResponseWriter, r *http.Request) {
	var tipoMuestra models.TipoMuestra
	vars := mux.Vars(r)

	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		common.JsonError(w, err, http.StatusBadRequest)
		return
	}

	tipoMuestra.ID = id
	err = models.TipoMuestras.Delete(tipoMuestra)
	if err != nil {
		common.JsonError(w, err, http.StatusNotFound)
		return
	}

	common.JsonStatus(w, http.StatusOK)
}
